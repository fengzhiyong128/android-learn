package cn.fzy.kotlin_learn

class MainClass {

    fun main(args: Array<String>) {
        if (args.size == 0) return
        print("First argument: ${args[0]}")
    }

    fun sun(a:Int , b:Int) : Int{
        return a + b
    }

    fun sun2(a:Int,b:Int)  = a + b

    fun printSum(a:Int,b:Int):Unit{
        print(a+b)
    }



}
